# Kustomization Practice

`kubectl` is required

## configMapGenerator
1. Create `application.properties` file
2. Create `kustomization.yaml`
3. Create ConfigMap by following command
    ```bash
    cd configMapGenerator
    kubectl apply -k .
    ```

## example-1
1. Create `base` folder to hold the basic manifests. This base can be used in multiple overlays.
2. Create `overlays/stg` & `overlays/stg`. Create variants for the targetd resources. e.g. `cpu_count.yaml` - cpu is different from that in `base/deployment.yaml` 
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: nginx
    spec:
      template:
        spec:
          containers:
            - name: nginx
              resources:
                requests:
                  cpu: "300m"
                limits:
                  cpu: "600m"
    ```
3. Create `kustomization.yaml` for each overlay
4. Go to specific folder and run below
    ```bash
    cd example-1/overlays/stg
    kubectl create ns stg
    kubectl apply -k .
    ```

    ```bash
    cd example-1/overlays/prod
    kubectl create ns prod
    kubectl apply -k .
    ```

    Deployments & Services will be created in `stg` & `prod` namespaces repectively
5. Delete all resources from `overlays/stg/kustomization.yaml`
    ```bash
    kubectl delete -k example-1/overlays/stg
    ```